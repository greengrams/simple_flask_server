install virtualenv (only first deploy need)
`pip install virtualenv`

make virtualenv folder with python3 (only first deploy need)
`virtualenv -p python3 .env`

use virtualenv folder as your environment
`source .env/bin/activate`

install package in environment (only first deploy need)
`pip install -r requirements.txt`

run the server
`gunicorn -b 127.0.0.1:8080 -t 30 --workers 1 run:app`

open the browser and try the url in web_api

ctrl+c to stop server

exit virtualenv
`deactivate`
# -*- coding: utf-8 -*-
from run import app

from flask import request, jsonify

@app.route('/test_restful/<test_str>', methods=['GET', 'POST'])
def test_restful_api(test_str):
    '''
    http://127.0.0.1:8080/test_restful/yooooo
    '''
    json_obj = {'status':test_str}
    response = jsonify(json_obj)
    response.status_code = 200
    return response

@app.route('/test_req', methods=['GET', 'POST'])
def test_req_api():
    '''
    http://127.0.0.1:8080/test_req?test_str=yooooo
    '''
    test_str = request.values['test_str'] 
    json_obj = {'status':test_str}
    response = jsonify(json_obj)
    response.status_code = 200
    return response
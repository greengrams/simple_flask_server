# -*- coding: utf-8 -*-
#! /usr/bin/env python
import sys

from flask import Flask
from flask_restful import Api

app = Flask(__name__)
api = Api(app)

import web_api

if __name__ == '__main__':
    app.run('0.0.0.0', port=8080, debug=True)

# gunicorn -b 127.0.0.1:8080 -t 30 --workers 1 run:app